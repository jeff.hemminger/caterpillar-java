package org.kropek.exercises.caterpillar;

import java.util.LinkedList;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
 * User: jeff
 * Date: 8/6/13
 * Time: 3:21 PM
 */
public class CaterPillarTree {

    List<Node> nodes;
    int stringIndex = -1; // the point within a given string
    int listIndex = 0; // the point in the list of strings
    List<String> stringList;

    public CaterPillarTree() {
        nodes = new LinkedList<Node>();
    }

    public static void main (String[] args) {
        CaterPillarTree tree = new CaterPillarTree();
//        String[] input = new String[]{"11101011111010010001000100"};
//        String[] input = new String[]{"1111100100110000"};
        String[] input = {"1111100000", "1111100000", "1111100000", "1111100000", "1111100000"};
//        String[] input = {"1","0"};
//        String[] input = {"11110100111100100111100110100110001110101001111000", "1101100000011100110000111001101100010000"};

        System.out.printf("Result %d%n", tree.fewestRemovals(newArrayList(input)));
    }

    public int fewestRemovals(List<String> input) {

        this.stringList = input;
        Node initialNode = new Node(0, null);
        nodes.add(initialNode);
        eval(initialNode);
        int best = 0;

        for (Node node : nodes) {
            best = Math.max(best, node.max1+node.max2+node.more+1);
        }

        return nodes.size() - best;

    }

    void eval(Node node) {

        stringIndex++;

        if(!canProceed()) {
            return;
        }

        while(stringList.get(listIndex).charAt(stringIndex) == '1') {
            Node n2 = new Node(node.max1 + 1, node);
            nodes.add(n2);
            eval(n2);
            node.add(n2);
            stringIndex++;

            if(!canProceed()) {
                return;
            }
        }
    }

    private boolean canProceed() {
        if(stringIndex >=  stringList.get(listIndex).length()) {
            stringIndex = 0;
            listIndex++;
        }
        return listIndex >= stringList.size();
    }

}
