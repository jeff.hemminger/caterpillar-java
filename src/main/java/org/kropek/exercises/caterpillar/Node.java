package org.kropek.exercises.caterpillar;

/**
 * User: jeff
 * Date: 8/6/13
 * Time: 3:31 PM
 */
public class Node {

    int pathPosition;
    int max1;
    int max2;
    int more;
    Node n1;
    Node n2;

    public Node(int place, Node reference) {
        this.max1 = place;
        this.max2 = 0;
        this.pathPosition = place;
        this.n1 = reference;
        this.more = 0;
    }

    public void add(Node n) {

        int c;

        if(n.max1 == n.pathPosition) {
            c = n.max2;
        } else {
            c = n.max1;
        }

        c += n.more + 1;

        if (max2 > 0) {
            more++;
        }

        if(c > max1) {
            max2 = max1;
            max1 = c;
            n2=n1;
            n1 = n;
        } else if(c > max2) {
            max2 = c;
            n2 = n;
        }
    }
}
